# Introduction

Hello! This is _soupless_, and I am a student using GitLab for personal reasons. Slowly migrating from GitHub.

You might know me from [Math Stack Exchange](https://math.stackexchange.com/users/888233), or from [GitHub](https://github.com/soupless). Yes, that's me.

# Where are the repositories?

I keep them private. If there are public repositories, I feel proud sharing it.

# Notes

- [`MTHC221`](https://github.com/soupless/MTHC221): An Abstract Algebra course (AY 2023-2024, Second Semester)
- [`MTHC115`](https://gitlab.com/soupless/MTHC115): A Linear Algebra course (AY 2023-2024, First Semester)